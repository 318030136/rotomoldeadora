# Bio rotomoldeadora

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Bio rotomoldeadora es una máquina rotomoldeadora para fabricar geometrías cerradas y huecas de biomaterial. Es importante considerar que la deshidratación del biomaterial posterior al rotomoldeado hará que disminuya el tamaño de la geometría obtenida. El flujo de trabajo se inicia con la Biomixer, donde se generan mezclas precisas que luego se vierten en el molde que girará de forma controlada. 

Este proyecto, es parte de la iniciativa https://gitlab.com/fablab-u-de-chile/NBD liderada por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional Materiom, y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile.

<img src="/IMG_5190__1_.jpg" width="430"> <img src="/Chanchito.png" width="430">

## Atributos

- Transmisión por correa.
- Estructura de perfiles T-Slot.
- Pocas partes impresas.
- Máx X RPM del marco interior.
- Soporte ajustable para moldes, de V-Slot 20x20.

## Cómo construirlo

- [LISTADO](parts.md) de partes, piezas y herramientas. 
- [Partes CAD](parts/).
- [ELECTRÓNICA](elec.md).
- [Instrucciones de ensamble](assembly.md).

<img src="/img/isorot.png" width="500">

## Trabajo futuro

- Programar rutina del proceso en función de velocidad máxima y tiempo de rotación.
- Construir interfaz usuario.
- Implementar planchas de refuerzo en el chasis.
- Construir módulo de electrónica.

### Ponte en contacto

Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
