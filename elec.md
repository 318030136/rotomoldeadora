# Electrónica

---

## Listado de componentes

 ITEM              | Cantidad
 ---------------------------   | ------------
Arduino UNO | 1
Fuente 12V 30A | 1
Modulo Encoder | 1
Botón pulsador | 1
Display LED 7 segmentos 4 dígitos | 1
Módulo MOSFET | 1
Módulo AC C14 | 1

+ Cables de conexión.



