# Partes y piezas

## Partes comerciales

 ITEM              | Cantidad
 ---------------------------   | ------------
 Motor ATEX 45 RPM 60Ncm  | 1
 Ángulo de Union T-Slot 30x30 |
 Tuerca T 30x30 M5 | 
 Polea GT2 30 dientes, DI 8 mm | 2
 Polea GT2 20 dientes, DI 8 mm | 2
 Polea GT2 sin dientes, D 20 mm | 2
 Eje 8 mm | 50 cm
 Rodamiento KFL08 | 8
 Soporte SHF8 | 4
 Perfil T Slot 30x30x500 mm | 12
 Perfil T Slot 30x30x250 mm | 5
 Perfil V-Slot 20x20x250 mm | 2
 Tuerca T 20x20 M5 | 4

 NOTA: Considerando que los perfiles deben ser cortados, también es válido comprar perfiles de mayores largos y cortar.

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/).  

 ITEM              | Cantidad
 ---------------------------   | ------------
Soporte motor | 1
Soporte polea | 1
Soporte poleas | 1
Soporte poleas B | 1
Union soporte molde | 4

## Herramientas

 ITEM |                  
 --------------------------- |  
 Sierra |
 Llave allen 4 mm |
 Llave allen 3 mm |
 Llave allen 2 mm |
 Llave allen 1.5 mm |




